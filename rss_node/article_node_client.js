var {FeedFactory} = require("../rss_feed/feed_factory")
var {InterestingRssFeed} = require("../rss_feed/interesting_rss_feed")
var {ArticleClassifier} = require("../rss_feed/nlp/article_classifier")

var {getFileLinesWithNoComments} = require("../rss_feed/nlp/text_parsing")

let RssParser = require('rss-parser');

var fs = require("fs")

var axios = require("axios")

let BLACKLISTED_PATHS = [
    `${__dirname}/minimal-stop.txt`,
    `${__dirname}/terrier-stop.txt`
]

let INTERESTING_TOPICS_PATH = `${__dirname}/interesting_topics.json`

let {
    GET_NEW_ARTICLES_COMMAND,
    START_COMMAND
} = require("./ipc_commands")

let INTERESTING_RSS_FEED = null

function getBlackListedWords(){
    let blacklisted_words = BLACKLISTED_PATHS.reduce((flat_words_array, curr_path) => {
        flat_words_array.push(...getFileLinesWithNoComments(curr_path))
        return flat_words_array
    }, [])

    let blacklisted_dict = blacklisted_words.reduce((word_dict, curr_word) => {
        return {
            ...word_dict,
            [curr_word.trim()]: true
        }
    })
    return blacklisted_dict
}

async function getInterestingRssFeed(feed_path){
    let blacklisted_dict = getBlackListedWords()
    let interesting_words = JSON.parse(fs.readFileSync(INTERESTING_TOPICS_PATH))
    let xml_data = fs.readFileSync(feed_path)

    let rss_feed = await new FeedFactory(xml_data).newRssFeed(new RssParser())
    let article_classifier = new ArticleClassifier(interesting_words, blacklisted_dict)

    return new InterestingRssFeed(rss_feed, article_classifier, axios.create({timeout: 5000}))
}


async function handleStart(message){
    INTERESTING_RSS_FEED = await getInterestingRssFeed(message.feed_path)
}

async function getNewArticles(message){
    if(INTERESTING_RSS_FEED == null){
        return []
    }

    let new_articles = await INTERESTING_RSS_FEED.getNewArticles()
    console.log("got_articles", new_articles.length)

    process.send({
        type: GET_NEW_ARTICLES_COMMAND,
        articles: new_articles
    })
}

async function handleMessage(message){
    let type = message.type
    
    if(type == START_COMMAND){
        await handleStart(message)
    }

    if(type == GET_NEW_ARTICLES_COMMAND){
        await getNewArticles(message)
    }
}


async function main(){
    process.on("message", handleMessage)
}

main()
let cp = require("child_process")

let {
    GET_NEW_ARTICLES_COMMAND,
    START_COMMAND
} = require("./ipc_commands")

class RssNodeServer
{
    constructor(feed_path){
        this.feed_path = feed_path
        this.client = null
        this.article_callback = null
        this.initClient()

        this.client_handling_request = false
    }

    async initClient(){
        this.client = cp.fork(`${__dirname}/article_node_client`)
        this.client.on("message", this.onClientMessage.bind(this))

        this.client.send({
            type: START_COMMAND,
            feed_path: this.feed_path
        })
    }

    onClientMessage(message){
        if(this.article_callback){
            this.article_callback(message)
        }
        this.client_handling_request = false
    }

    onArticles(articles_callback){
        this.article_callback = articles_callback
    }

    kill(){
        this.client.kill("SIGKILL")
    }

    async getNewArticles(){
        if(!this.client || this.client_handling_request) return

        this.client_handling_request = true
        this.client.send({
            type: GET_NEW_ARTICLES_COMMAND
        })
    }
}

exports.RssNodeServer = RssNodeServer
var {RssFeed} = require("./rss_feed")
var {ArticleClassifier} = require("./nlp/article_classifier")
var axios = require("axios")
var {cleanHtml} = require("./nlp/text_parsing")

class InterestingRssFeed
{
    constructor(rss_feed, article_classifier, http_client){
        this.rss_feed = rss_feed
        this.article_classifier = article_classifier

        this.http_client = http_client

        this.higest_score = 0
        this.avg_score = 0
        this.avg_size = 0
    }

    threshold(){
        return this.avg_score + (this.higest_score - this.avg_score) / (Math.max(this.higest_score, 4))
    }

    async getArticleGrade(article){

        try{
            let article_page = await this.http_client.get(article.link)
            article_page = cleanHtml(article_page.data).toLowerCase()
    
            let grades = await this.article_classifier.parseText(article_page)
    
            let grades_sum = grades.reduce((acc, grade) => {
                return acc + grade.grade
            }, 0)
    
            return {article: article, grade: grades_sum}
        } catch {
            return null
        }
        
    }

    handleArticleGrades(article_grades){

        let avg_grade = article_grades.reduce((sum_grade, {grade}) => {
            return sum_grade + (grade || 0)
        }, 0) / article_grades.length

        this.higest_score = article_grades.reduce((higest_grade, {grade}) => {
            return higest_grade > grade ? higest_grade : grade
        }, this.higest_score)

        this.avg_score = (this.avg_score * this.avg_size + avg_grade * article_grades.length) / (this.avg_size + article_grades.length)
        this.avg_size += article_grades.length
    }


    async getNewArticles(){
        let new_articles = await this.rss_feed.getNewArticles()
        if(new_articles.length == 0) return []

        let article_grades = await Promise.all(new_articles.map(this.getArticleGrade.bind(this)))
        article_grades = article_grades.filter(article_grade => article_grade != null)

        if(article_grades.length == 0) return []
        this.handleArticleGrades(article_grades)

        console.table([{
            avg_score: this.avg_score,
            avg_size: this.avg_size,
            higest_score: this.higest_score
        }])

        return article_grades.filter(({grade}) => grade > this.threshold())
    }
}

exports.InterestingRssFeed = InterestingRssFeed

var xml2json = require('xml2js').parseString
var {RssFeed} = require("./rss_feed")

class FeedFactory
{
    constructor(xml_data){
        this.xml_data = xml_data
    }

    extractUrlsFromFile(groups)
    {
        return groups.reduce((acc, group) => {
            if(group.outline) acc.push(...this.extractUrlsFromFile(group.outline))
            else acc.push(group["$"].xmlUrl)
            return acc
        }, [])
    }

    getUrls()
    {
        return new Promise((resolve, reject) => {
            xml2json(this.xml_data, (err, items) => {

                if (err) {
                  return reject(err)
                }
            
                var groups = items.opml.body[0].outline
                resolve(this.extractUrlsFromFile(groups))
            });
        })
    }

    async newRssFeed(rss_parser){
        let urls = await this.getUrls()
        return new RssFeed(rss_parser, urls)
    }
}

exports.FeedFactory = FeedFactory


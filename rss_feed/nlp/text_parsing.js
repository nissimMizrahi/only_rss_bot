
var fs = require("fs")

exports.getFileLinesWithNoComments = function(path)
{
    let text = fs.readFileSync(path).toString()
    return text
    .split("\n")
    .filter(line => line.length > 0 && line[0] != "#")
    .map(line => line.toLowerCase())
}

exports.cleanHtml = function cleanHtml(html)
{
    return html
        .replace(/<head [\S\s]*?>([\S\s]*?)<\/head>/g, "")
        .replace(/<script[\S\s]*?>[\S\s]*?<\/script>/g, "")
        .replace(/<style[\S\s]*?>[\S\s]*?<\/style>/g, "")
        .replace(/<meta[\S\s]*?>[\S\s]*?>/g, "")
        .replace(/<link[\S\s]*?>[\S\s]*?>/g, "")
        .replace(/<noscript[\S\s]*?>[\S\s]*?<\/noscript>/g, "")
        .replace(/<iframe[\S\s]*?>[\S\s]*?<\/iframe>/g, "")
        .replace(/<head[\S\s]*?>[\S\s]+<\/head>/g, "")
        .replace(/<[\S\s]*?>/g, "")
        .replace(/\s+/g, " ")
        .replace(/\s+/g, " ")
}
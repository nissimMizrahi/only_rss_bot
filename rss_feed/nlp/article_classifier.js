const natural = require("natural")

class ArticleClassifier {
    constructor(interesting_words, blacklist_dict) {
        this.interesting_words = interesting_words
        this.blacklist_dict = blacklist_dict
    }

    getWord(tfidf, word)
    {
        return new Promise((resolve, reject) => {
            tfidf.tfidfs(word, (i, measure) => {
                resolve({word: word, grade: measure});
            });
        })
    }

    getTfidf(text)
    {     
        let tfidf = new natural.TfIdf();
        let tokenizer = new natural.WordTokenizer()


        let tokens = tokenizer.tokenize(text)
        .filter(word => !this.blacklist_dict[word])

        tfidf.addDocument(tokens);
        return tfidf
    }

    async parseText(text)
    {
        try{
            let tfidf = this.getTfidf(text)
            return await Promise.all(this.interesting_words.map(word => this.getWord(tfidf, word)))
        } catch {
            return []
        }
    }
}

exports.ArticleClassifier = ArticleClassifier


class RssFeed
{
    constructor(rss_parser, urls){
        this.urls = urls
        this.visitedArticles = {}  
        this.parser = rss_parser
    }

    async parseUrl(url){

        try{
            let rss_feed = await this.parser.parseURL(url)
            let not_yet_visited_articles = rss_feed.items.filter(item => !this.visitedArticles[item.link])
    
            not_yet_visited_articles.map(article => {
                this.visitedArticles[article.link] = true
            })
    
            return not_yet_visited_articles
        } catch {
            return []
        }
    }

    async getNewArticles(){
        let new_articles = await Promise.all(
            this.urls.map(this.parseUrl.bind(this))
        )

        new_articles = new_articles.reduce((flat_array, article_array) => {
            flat_array.push(...article_array)
            return flat_array
        })

        return new_articles
    }
}

exports.RssFeed = RssFeed
const si = require('systeminformation');

exports.getRam = () => {
    return si.mem()
}

exports.getTemp = () => {
    return new Promise((resolve, reject) => {
        si.cpuTemperature().then(temp => {
            resolve(temp.main)
        }).catch(reject)
    })
}

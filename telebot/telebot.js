
const Telegraf = require('telegraf')

let {getRam, getTemp} = require("./system_utils")

let {RssNodeServer} = require("../rss_node/article_node_server")
let fs = require("fs")
let cp = require("child_process")

let FEEDS_DIR = `${__dirname}/../feeds`


let articles_interval = null
let article_subscriptions = {}

function broadcastMessageToSubscriptions(telegram_bot, text){
    Object.keys(article_subscriptions).map(chat_id => {
        telegram_bot.telegram.sendMessage(chat_id, text)
    })
    
}

function initRssNodes(telegram_bot, feeds_dir){
    let feed_files = fs.readdirSync(feeds_dir)
    let nodes = feed_files.map(file => new RssNodeServer(`${feeds_dir}/${file}`))

    nodes.map(rss_server => {
        rss_server.onArticles((article_message) => handleNewArticles(telegram_bot, article_message))
    })

    return nodes
}

function handleNewArticles(telegram_bot, articles_message){
    let new_articles = articles_message.articles.map(article => article.article)

    new_articles.forEach(article => {
        broadcastMessageToSubscriptions(telegram_bot, article.link)
    })
}

function getNewArticles(nodes){
    nodes.map(rss_server => {
        rss_server.getNewArticles()
    })
}






async function mesureTemp(ctx) {

    try{
        let temp = await getTemp()
        ctx.reply(temp)
    }
    catch{
        ctx.reply("unable to mesure temp")
    }
}

async function ramUsage(ctx) {
    try{
        let mem = await getRam()
        ctx.reply(`free: ${mem.free / (1000 * 1000 * 1000)} GB, swap free: ${mem.swapfree / (1000 * 1000 * 1000)} GB`)
    }
    catch{
        ctx.reply("unable to mesure cpu")
    }
}

function start_article_flow(nodes){
    articles_interval = setInterval(() => getNewArticles(nodes), 5000)
}

function stop_article_flow(){
    clearInterval(articles_interval)
    articles_interval = null
}

function subscribe_to_flow(ctx){
    article_subscriptions[ctx.message.chat.id] = true
}

function unsubscribe_to_flow(ctx){
    delete article_subscriptions[ctx.message.chat.id]
}


function restart(nodes){
    setTimeout(() => {
        cp.spawn(process.argv.shift(), process.argv, {
            cwd: process.cwd(),
            detached : true,
            stdio: "inherit"
        })

        nodes.map(node => node.kill())
        process.exit(0)
    }, 2000)
    
}

function initTelegramBot(telegram_bot, rss_nodes){

    const commands = {

        start_flow: { func: () => start_article_flow(rss_nodes), description: "globally starts article flow, doesnt subscribe" },
        stop_flow: { func: stop_article_flow, description: "globally stops article flow, doesnt unsubscribe" },
    
        subscribe_flow: {func: subscribe_to_flow, description: "subscribe chat to article flow"},
        unsubscribe_flow: {func: unsubscribe_to_flow, description: "unsubscribe chat to article flow"},

        temp: { func: mesureTemp, description: "get temperature" },
        ram: {func: ramUsage, description: "gets memory info"},
        restart: {func: () => restart(rss_nodes), description: "restart bot"},
    }

    Object.entries(commands).map(([command, handler]) => {
        telegram_bot.command(command, handler.func)
    })
    
    
    telegram_bot.telegram.setMyCommands(Object.entries(commands).map(([command, handler]) => {
        return {
            command: command,
            description: handler.description
        }
    }))

}

function initMemoryCheck(telegram_bot, nodes){
    setInterval(async () => {
        let memory =  (await getRam()).free
        console.log("ram check", memory)

        if(memory < 1000 * 1000 * 500){
            broadcastMessageToSubscriptions(telegram_bot, "low on ram, restarting")
            restart(nodes)
        }
    }, 1000 * 60)
}


async function main()
{
    try {
        let telegram_bot = new Telegraf(process.env.RssaselKey)
        let nodes = initRssNodes(telegram_bot, FEEDS_DIR)
    
        initMemoryCheck(telegram_bot, nodes)
        initTelegramBot(telegram_bot, nodes)
        telegram_bot.launch()
    } catch(e) {
        console.log(e)
    }    
}

main()


//console.log(await telegram_bot.telegram.getWebhookInfo())
//console.log(await telegram_bot.telegram.getUpdates({
//    offset: 411067853
//}))